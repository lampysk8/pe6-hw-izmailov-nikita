"use strict";

const API = `https://ajax.test-danit.com/api/json/`,
   root = document.querySelector("#root");

const sendRequest = async (url, method = "GET", config) => {
   return await fetch(url, {
      method,
      body: config,
   }).then((response) => {
      if (response.ok) {
         if (method === "DELETE") {
            return response;
         } else {
            return response.json();
         }
      } else {
         return new Error("Something wrong!")
      }
   })
}

const authors = () => sendRequest(`${API}users`);
const posts = () => sendRequest(`${API}posts`);
const deletePost = (id) => sendRequest(`${API}posts/${id}`, "DELETE");

const postsArr = async () => {
   const cards = []

   await Promise.all([authors(), posts()]).then((data) => {
      const [authorsArr, postsArr] = data;
      authorsArr.forEach(author => {
         postsArr.forEach(post => {
            if (author.id === post.userId) {
               const card = new Card({
                  title: post.title,
                  text: post.body,
                  author: author.name,
                  email: author.email,
                  cardId: post.id,
                  userId: author.id
               });
               cards.push(card);
            }
         })
      });
   })
   return cards;
}

class Card {
   constructor({ title, text, author, email, cardId, userId }) {
      this.title = title,
         this.text = text,
         this.author = author,
         this.email = email,
         this.cardId = cardId,
         this.userId = userId
   }
   render() {
      this.card = document.createElement("div");
      this.card.classList.add("card");
      this.card.setAttribute("data-post-id", this.cardId);
      this.card.setAttribute("data-user-id", this.userId);
      this.card.insertAdjacentHTML("afterbegin", `
         <h2 class="card-title">${this.title}</h2>
         <p class="card-text">${this.text}</p>
         <p class="card-author">Author: ${this.author}</p>
         <p class="card-author__email">Email: 
            <a href="mailto:${this.email}">${this.email}</a>
         </p>
            <button class="btn-del">Delete</button>
      `);
      return this.card;
   }
}
const deleteCard = () => {
   document.querySelector("#root").addEventListener("click", (e) => {
      if (e.target.classList.contains("btn-del")) {
         const delCard = e.target.closest(".card");
         deletePost(delCard.getAttribute("data-post-id"));
         delCard.remove();
      }
   });
};

const renderCards = (posts) => {
   posts.forEach(card => {
      root.append(card.render());
   });
   deleteCard();
}

postsArr().then((posts) => {
   renderCards(posts);
})



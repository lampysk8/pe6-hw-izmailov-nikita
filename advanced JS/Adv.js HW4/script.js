"use strict";

const API = `https://ajax.test-danit.com/api/swapi/films`;
const root = document.querySelector("#root");
const charactersArr = [];

const renderFilms = (films) => {
   const list = document.createElement("ul");
   list.classList.add("films");

   films.forEach(({ episodeId, name, openingCrawl, characters }) => {
      charactersArr.push(characters);
      list.insertAdjacentHTML("afterbegin", `
            <li class="films-card">
				   <h2 class="films-name">${name}</h2>
				   <p class="films-episode">Episode: ${episodeId}</p>
				   <p class="films-descr">
					${openingCrawl}
				   </p>
				   <div class="actors"></div>
            </li>
      `)

   });
   renderCharacters(charactersArr);
   root.append(list);

}

const getFilms = (url) => {
   fetch(url)
      .then(response => response.json())
      .then(renderFilms)
      .catch(err => alert(err.message))
}
getFilms(API);



const renderCharacters = (charactersArr) => {
   let index = 0;

   charactersArr.forEach((characters) => {
      const promiseArr = [];

      characters.forEach((characterUrl) => {
         promiseArr.push(fetch(characterUrl).then((response) => response.json()));
      });
      Promise.all(promiseArr)
         .then((charactersArr) => {
            const actors = document.querySelectorAll(".actors");
            const actorsList = document.createElement("ul");
            actorsList.classList.add("actors-list");

            charactersArr.forEach(({ name }) => {
               actorsList.innerHTML += `
      					<li class="actors-name">${name}</li>
      				`;
            });

            actors[index].innerHTML = `<h2 class="actors-title">Actors:</h2>`;
            actors[index].append(actorsList);
         })
         .catch((err) => alert(err.message))
         .finally(() => {
            index++;
         });
   });
};


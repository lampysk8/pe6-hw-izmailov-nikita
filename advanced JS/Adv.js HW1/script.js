"use strict";

class Employee {
   constructor(name, age, salary) {
      this.name = name;
      this.age = age;
      this.salary = salary;
   }
   get name() {
      return this._name;
   }
   set name(value) {
      if (value.length < 4) {
         console.log("name is too short");
         return;
      }
      this._name = value;
   }
   get age() {
      return this._age;
   }
   set age(value) {
      if (value < 16) {
         console.log("Are you sure?");
         return;
      }
      this._age = value;
   }
   get salary() {
      return this._salary;
   }
   set salary(value) {
      if (value === "") {
         console.log("You must specify your salary");
         return;
      }
      this._salary = value;
   }
}

class Programmer extends Employee {
   constructor(name, age, salary, lang) {
      super(name, age, salary)
      this.salary = salary;
      this.lang = lang;
   }
   get salary() {
      return this._salary * 3;
   }
   set salary(value) {
      if (value === "") {
         console.log("You must specify your salary");
         return;
      }
      this._salary = value;
   }
}

const user = new Employee("Maximus", 30, 16000);
const user2 = new Programmer("Stas", 20, 50000, ["js", "html", "css"]);
const user3 = new Programmer("Sta", 15, 10000);
console.log(user);
console.log(user.salary);
console.log(user2);
console.log(user2.salary);
console.log(user3);
console.log(user3.salary);



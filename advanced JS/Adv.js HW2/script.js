"use strict";

const books = [
   {
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70
   },
   {
      author: "Сюзанна Кларк",
      name: "Джонатан Стрейндж і м-р Норрелл",
   },
   {
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
   },
   {
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
   },
   {
      author: "Террі Пратчетт",
      name: "Рухомі картинки",
      price: 40
   },
   {
      author: "Анґус Гайленд",
      name: "Коти в мистецтві",
   }
];

const menu = document.querySelector("#root");

let list = books.map(({ author, name, price }, book) => {
   try {
      if (author === undefined) {
         throw new Error(`Нет автора в ${book + 1}-й книге`);
      }
      if (name === undefined) {
         throw new Error(`Нет названия в ${book + 1}-й книге`);
      }
      if (price === undefined) {
         throw new Error(`Нет цены в ${book + 1}-й книге`);
      }
      return `<li>Author: ${author}</br> name: ${name}</br> price: ${price}</li>`;
   } catch (err) {
      console.log(err);
   }
});
menu.insertAdjacentHTML("afterbegin", `<ol>${list}<ol>`);

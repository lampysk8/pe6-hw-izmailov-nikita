"use strict";

const btn = document.querySelector("button"),
   messageWrapper = document.querySelector("div");

const sendRequest = async (url) => {
   return await fetch(url).then((response) => {
      if (response.ok) {
         return response.json();
         // console.log(response.json());
      }
      return new Error("Something wrong");
   })
}

const getIp = () => sendRequest(`http://api.ipify.org/?format=json`);
const useIp = (ip) => sendRequest(`http://ip-api.com/json/${ip}?fields=3719455`);

btn.addEventListener("click", () => {
   getIp().then(({ ip }) => {
      useIp(ip).then((data) => {
         render(ip, data)
         console.log(data);
         console.log(ip);
      });
   });
});

const render = (ip, { continent, country, city, regionName, district }) => {
   messageWrapper.insertAdjacentHTML("afterbegin", `
      <h2>Your IP is: ${ip}</h2>
		<div>Continent: ${continent}</div>
		<div>Country: ${country}</div>
		<div>City: ${city}</div>
		<div>Region name: ${regionName}</div>
		<div>District: ${district ? district : "There is no district!"}</div>
   `);
};
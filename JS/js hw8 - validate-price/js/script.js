"use strict"

document.addEventListener("DOMContentLoaded", () => {
   const container = document.querySelector(".container");
   const input = document.createElement("input");
   const currentPrice = document.createElement("span");
   const wrongPrice = document.createElement("span");

   input.setAttribute("type", "text");
   // console.log(input.attributes);
   container.textContent = "Price: ";
   container.append(input);

   input.addEventListener("focus", onFocus);
   input.addEventListener("blur", onBlur);

   function onFocus() {
      this.classList.add("active");
      this.classList.remove("color-green");
      this.classList.remove("color-red");
      this.value = "";

      [...container.children].forEach(e => {
         if (e === currentPrice) {
            container.removeChild(currentPrice);
         } else if (e === wrongPrice) {
            container.removeChild(wrongPrice);
         }
      });
   }

   function onBlur() {
      this.classList.remove("active");

      if (isNaN(+input.value) || +input.value <= 0 || +input.value === "") {
         this.classList.add("color-red");
         this.classList.remove("color-green");
         creatingWrongPrice();
         return;
      } else {
         this.classList.add("color-green");
         this.classList.remove("color-red");
         creatingPrice();
      }
   };

   function creatingPrice() {
      currentPrice.classList.add("price-out");
      currentPrice.textContent = `Текущая цена: ${input.value}`;

      const removePrice = document.createElement("span");
      removePrice.textContent = "X";
      removePrice.classList.add("price-remove");

      currentPrice.append(removePrice);
      container.prepend(currentPrice);

      removePrice.addEventListener("click", RemoveClickPrice);
   };
   function RemoveClickPrice(event) {
      container.removeChild(event.target.parentNode);
      input.value = "";
      input.focus();
   };
   function creatingWrongPrice() {
      wrongPrice.textContent = "Please enter correct price";
      wrongPrice.classList.add("price-wrong");
      wrongPrice.classList.add("color-red");

      container.prepend(wrongPrice);
   };
});


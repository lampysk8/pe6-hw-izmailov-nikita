"use strict"

let userName = prompt("What is your name?");
let userAge = +prompt("How old are you?");

if (userAge < 18) {
   console.log("You are not allowed to visit this website");
} else if (userAge >= 18 && userAge <= 22) {
   if (confirm("Are you sure you want to continue?")) {
      console.log(`Welcome, ${userName}`);
   } else {
      console.log("You are not allowed to visit this website");
   }
} else {
   console.log(`Welcome, ${userName}`);
}

// ! Optional

// Подскажите, как к prompt добавить ранее добавленную информацию из опционального задания? 

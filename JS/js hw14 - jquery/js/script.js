"use strict"

$(document).ready(function () {

   $(document).on(`click`, ".navigation_link", function (e) {
      e.preventDefault();
      $(`html`).animate({
         scrollTop: $($(this).attr('href')).offset().top - $(`.navigation`).height(),
      }, 500);
   });

   const btnTop = document.createElement(`button`);
   $(btnTop).text(`Наверх`);
   $(btnTop).css({
      position: `fixed`,
      bottom: `10px`,
      right: `20px`,
      padding: `10px 15px`,
      "background-color": `#313131`,
      color: `#ffffff`,
      cursor: `pointer`,
   });
   $(`body`).append(btnTop);

   if ($(window).scrollTop() >= $(window).height()) {
      $(btnTop).css(`display`, `block`);
   } else $(btnTop).css(`display`, `none`);

   $(window).scroll(function () {
      if ($(this).scrollTop() >= $(window).height()) {
         $(btnTop).css(`display`, `block`);
      } else $(btnTop).css(`display`, `none`);
   });

   $(btnTop).click(function (e) {
      e.preventDefault();
      $(`html`).animate({
         scrollTop: 0,
      }, 500);
   });

   const btnHide = document.createElement(`button`);
   $(btnHide).text(`Hide section`);
   $(btnHide).css({
      display: `block`,
      margin: `0 auto`,
      padding: `10px 15px`,
      color: `#ffffff`,
      "background-color": `#000000`,
      cursor: `pointer`
   });

   $(`.news`).append(btnHide);

   $(btnHide).click(function (e) {
      e.preventDefault();

      if ($(btnHide).text() === `Hide section`) {
         $(btnHide).text(`Show section`);
      } else $(btnHide).text(`Hide section`);

      $(`.news_list`).slideToggle(`slow`);
   });

});


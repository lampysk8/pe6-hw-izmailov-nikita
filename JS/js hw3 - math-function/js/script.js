"use strict"


// let x;



function isNumber(x) {
   return (!(!x || isNaN(+x) || typeof +x !== "number"));
}
isNumber();
// console.log(isNumber(0)); // false
// console.log(isNumber(isNaN(0))); // false
// console.log(isNumber(isNaN("12asd"))); // true
// console.log(isNumber(isNaN("12a"))); // true
// console.log(isNumber(isNaN(12))); // false 

let a, b, c;
do {
   a = +prompt("Enter first number", a);
   b = +prompt("Enter second number", b);
   c = prompt("Enter operation +, -, *, /");
} while (!isNumber(a) && !isNumber(b));
function calcArguments(a, b, c) {
   switch (c) {
      case "+":
         return a + b;
      case "-":
         return a - b;
      case "*":
         return a * b;
      case "/":
         if (b !== 0) {
            return a / b;
         } else {
            return ("Do not divide by 0");
         }
   }
};
console.log(calcArguments(a, b, c));


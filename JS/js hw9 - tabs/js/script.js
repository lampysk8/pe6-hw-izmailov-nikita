"use strict"

document.addEventListener("DOMContentLoaded", () => {
   const tab = document.querySelector(".tabs");
   const tabsTitle = [...document.querySelectorAll(".tabs-title")];
   const tabsContent = [...document.querySelectorAll(".tabs-content li")];

   tabsContent.forEach(e => {
      if (tab.classList.contains("active")) {
         tabsContent.forEach(tabsContent => {
            if (e.getAttribute("data-tabs") === tabsContent.getAttribute("data-tabs")) {
               tabsContent.style.display = "block";
            }
         });
      }
   });
   tab.addEventListener("click", onClickTab);

   function onClickTab(event) {
      tabsTitle.forEach(elem => {
         elem.classList.remove("active");
      });
      event.target.classList.add("active");

      tabsContent.forEach(element => {
         element.style.display = "none";
         if (element.getAttribute("data-tabs") === event.target.getAttribute("data-tabs")) {
            element.style.display = "block";
         }
      });
   }
});
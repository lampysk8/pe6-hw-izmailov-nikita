"use strict"

document.addEventListener("DOMContentLoaded", () => {
   const pass = document.querySelector(".pass");
   const passRepeat = document.querySelector(".pass-repeat");
   const passIcon = document.querySelector(".icon-password");
   const passRepeatIcon = passRepeat.nextElementSibling;
   const btn = document.querySelector(".btn");
   const wrongPass = document.createElement("span");

   btn.addEventListener("click", onBtnClick);
   passIcon.addEventListener("click", replaceIcon);
   passRepeatIcon.addEventListener("click", replaceRepeatIcon);

   function addToggle(elem) {
      elem.classList.toggle("fa-eye");
      elem.classList.toggle("fa-eye-slash");
   }

   function replaceIcon() {
      if (pass.getAttribute("type") === "password") {
         pass.setAttribute("type", "text");
         addToggle(passIcon);
      } else {
         pass.setAttribute("type", "password");
         addToggle(passIcon);
      }
   }

   function replaceRepeatIcon() {
      if (passRepeat.getAttribute("type") === "password") {
         passRepeat.setAttribute("type", "text");
         addToggle(passRepeatIcon);
      } else {
         passRepeat.setAttribute("type", "password");
         addToggle(passRepeatIcon);
      }
   }

   function onBtnClick(event) {
      event.preventDefault();
      if (pass.value === passRepeat.value && pass.value !== '') {
         alert("You are welcome!");
      } else {
         wrongPass.textContent = "Нужно ввести одинаковые значения!";
         wrongPass.style.color = "red";
         passRepeat.after(wrongPass);
      }
   }

});

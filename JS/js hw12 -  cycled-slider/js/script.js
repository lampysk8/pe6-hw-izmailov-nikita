"use strict"

const images = [...document.querySelectorAll(".image-to-show")];
const btnStart = document.querySelector(".btn-start");
const btnStop = document.querySelector(".btn-stop");
let index = 0;
let timer;

function sliderImages() {
   timer = setInterval(() => {
      images.forEach((elem, i) => {
         if (elem.classList.contains("active")) {
            if (i === images.length - 1) {
               index = 0;
            } else {
               index = i + 1;
            }
         }
         elem.classList.remove("active");
      });

      images[index].classList.add("active");
   }, 2000);
}

sliderImages();

btnStart.addEventListener("click", () => {
   clearInterval(timer);
});
btnStop.addEventListener("click", () => {
   clearInterval(timer);
   sliderImages();
});




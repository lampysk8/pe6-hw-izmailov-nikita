"use strict"


const name = prompt("Enter your name");
const surName = prompt("Enter your surname");
const birthday = new Date(prompt("How old are you?", "dd.mm.yyyy"));

function createNewUser(name, surname, age) {
   let result = {
      firstName: name,
      lastName: surname,
      birthday: age,
      getLogin() {
         return (this.firstName.slice(0, 1).toUpperCase()) + "" + (this.lastName.toLowerCase());
      },
      getAge() {
         return Math.floor((Date.parse(new Date()) - Date.parse(this.birthday)) / (1000 * 60 * 60 * 24 * 364.25))
      },
      getPassword() {
         return this.getLogin() + "" + this.birthday.getFullYear();
      },
   };

   return result;
}
const newUser = createNewUser(name, surName, birthday);

// console.log(newUser.firstName);
// console.log(newUser.lastName);
// console.log(((newUser.getLogin()).toLowerCase()));
// console.log(newUser.getLogin());
// alert(` Your age is: ${newUser.getAge()}`);
console.log(` Your age is: ${newUser.getAge()}`);
console.log(newUser.getPassword());







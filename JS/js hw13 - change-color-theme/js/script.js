"use strict"

document.addEventListener("DOMContentLoaded", () => {

   const btn = document.querySelector(".toggle-theme");
   const styleFile = document.querySelector("#toggle-theme-color");
   let colorFlag;

   function setThemeColor() {
      const themeColor = localStorage.getItem("themeColor");
      if (!themeColor || themeColor === "light") {
         styleFile.href = "./style/light.css";
         colorFlag = "light";
      }
      if (themeColor === "dark") {
         styleFile.href = "./style/dark.css";
         colorFlag = "dark";
      }
   }
   setThemeColor();

   btn.addEventListener("click", btnOnCLick);

   function btnOnCLick() {
      if (colorFlag === "light") {
         localStorage.setItem("themeColor", "dark");
         setThemeColor();
      } else if (colorFlag === "dark") {
         localStorage.setItem("themeColor", "light");
         setThemeColor();
      }
   };
});

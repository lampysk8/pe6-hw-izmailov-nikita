"use strict"

const buttons = [...document.querySelectorAll(".btn")];
console.log(buttons);

document.addEventListener("keydown", onKeyDown);

function onKeyDown(e) {
   buttons.forEach(button => {
      if (e.key === button.getAttribute("data-btn")) {
         buttons.forEach(button => {
            button.classList.remove("blue");
         });
         button.classList.add("blue");

      }
   });
}
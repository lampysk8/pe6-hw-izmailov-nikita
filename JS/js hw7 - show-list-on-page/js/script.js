"use strict"

function createList(array, parent = document.body) {

   const list = document.createElement("ul");

   array.forEach(element => {

      const listItem = document.createElement("li");

      if (Array.isArray(element)) {
         createList(element, listItem);
      } else {
         listItem.textContent = element;
      }

      list.append(listItem);
   });

   parent.append(list);
}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"]);




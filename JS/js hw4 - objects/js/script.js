"use strict"

// ! without optional

const name = prompt("Enter your name");
const surName = prompt("Enter your surname");

function createNewUser(name, surname) {
   let result = {
      firstName: name,
      lastName: surname,
      getLogin() {
         return this.firstName.slice(0, 1) + "" + this.lastName;
      },
   };

   return result;
}
const newUser = createNewUser(name, surName);

console.log(newUser.firstName);
console.log(newUser.lastName);
console.log(((newUser.getLogin()).toLowerCase()));

// // ! with optional

// const name = prompt("Enter your name");
// const surName = prompt("Enter your surname");


// function createNewUser(name, surname) {
//    let result = {
//       firstName: name,
//       lastName: surname,
//       getLogin() {
//          return this.firstName.slice(0, 1) + "" + this.lastName;
//       },
//    };
//    Object.definePropertiy(newUser, {
//       "firstName": {
//          setFirstName() {

//          }
//       }, "lastName": {
//          setLastName() {

//          }
//       }
//    });

//    return result;
// }
// const newUser = createNewUser(name, surName);


// console.log(newUser);

// console.log(newUser.firstName);
// console.log(newUser.lastName);
// console.log(((newUser.getLogin()).toLowerCase()));





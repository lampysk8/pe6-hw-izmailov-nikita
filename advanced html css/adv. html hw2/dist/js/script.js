"use strict";

function menuActive() {
   let burgerMenu = document.querySelector('.header__menu-burger');
   let menuList = document.querySelector('.menu');

   burgerMenu.addEventListener('click', () => {
      burgerMenu.classList.toggle("close");
      menuList.classList.toggle("overlay");
   });
};
menuActive();

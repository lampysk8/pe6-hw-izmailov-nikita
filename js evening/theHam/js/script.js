"use strict"

function Services() {
   const menu = document.querySelectorAll(".services-item"),
      titles = document.querySelectorAll(".service-item_wrapper");

   menu.forEach(element => {
      element.addEventListener("click", () => {
         let currentBtn = element;
         let tabId = currentBtn.getAttribute("data-tab");
         let currentTab = document.querySelector(tabId);

         if (!currentBtn.classList.contains("services-item_active")) {
            menu.forEach(menuItem => {

               menuItem.classList.remove("services-item_active");
               menuItem.lastElementChild.classList.remove("services-link_active");

               titles.forEach(title => {
                  title.classList.remove("service-item_wrapper__active");
               });
            })
            currentBtn.classList.add("services-item_active");
            currentBtn.lastElementChild.classList.add("services-link_active");
            currentTab.classList.add("service-item_wrapper__active");
         }
      })
   });
}



//event target delete after use 

document.addEventListener("DOMContentLoaded", function onClick() {
   document.addEventListener("click", (e) => console.log(e.target));
});
// event target !!!


const images = [
   [{
      data: "web-design",
      src: "./images/portfolio/img_1.jpg"
   }, {
      data: "web-design",
      src: "./images/portfolio/img_2.jpg"
   }, {
      data: "web-design",
      src: "./images/portfolio/img_3.jpg"
   }, {
      data: "graph-design",
      src: "./images/portfolio/img_5.jpg"
   }, {
      data: "graph-design",
      src: "./images/portfolio/img_6.jpg"
   }, {
      data: "graph-design",
      src: "./images/portfolio/img_7.jpg"
   }, {
      data: "wordpress",
      src: "./images/portfolio/img_9.jpg"
   }, {
      data: "wordpress",
      src: "./images/portfolio/img_10.jpg"
   }, {
      data: "wordpress",
      src: "./images/portfolio/img_11.jpg"
   }, {
      data: "land-page",
      src: "./images/portfolio/img_12.jpg"
   }, {
      data: "land-page",
      src: "./images/portfolio/img_13.jpg"
   }, {
      data: "land-page",
      src: "./images/portfolio/img_14.jpg"
   }],
];

function Portfolio() {

   const
      galleryMenu = document.querySelector(".portfolio-menu"),
      container = document.querySelector(".portfolio"),
      btnAddMore = document.querySelector(".btn-green-load"); // 'btn' add more
   let count = 0; // count for new images

   console.log(btnAddMore.lastElementChild);

   // Portfolio menu and filter cards

   galleryMenu.addEventListener('click', (event) => {

      let dataMenu = null;
      const imagesContainer = document.querySelectorAll(".portfolio-container"); // 'div' with a block(data-filter)

      if (event.target.nodeName === "BUTTON") {
         dataMenu = event.target.getAttribute('data-btn');
      }

      let menuItems = document.querySelectorAll('[data-btn]');

      menuItems.forEach(item => {
         if (item.classList.contains('work-menu_link__active')) {
            item.classList.remove('work-menu_link__active');
         }
      });

      if (!event.target.classList.contains('work-menu_link__active')) {
         event.target.classList.add('work-menu_link__active');
      }

      imagesContainer.forEach((item) => {
         if (dataMenu !== item.getAttribute('data-filter') && dataMenu !== 'all') {
            item.style.display = 'none'
         } else {
            item.style.display = 'block'
         }
      })
   })

   btnAddMore.addEventListener("click", () => {
      let spanLoader = document.createElement("span");
      btnAddMore.append(spanLoader);

      btnAddMore.firstElementChild.style.display = "none";
      spanLoader.classList.add("loader");

      setTimeout(() => {

         images[count].forEach((img) => {
            container.insertAdjacentHTML("beforeend", `
     <div class="portfolio-container" data-filter="${img.data}">
            <img class="portfolio-img image-section" src="${img.src}" alt="picture" width="290px"
               height="217px">
            <div class="portfolio-style">
               <a class="portfolio-link__btn-chain" href="javascript://">
                  <svg width="41" height="40" viewBox="0 0 42 42" fill="none" xmlns="http://www.w3.org/2000/svg">
                     <rect x="1" y="1" width="41" height="40" rx="20" stroke="#18CFAB" />
                     <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M26.9131 16.7282L25.0948 14.8913C24.2902 14.0809 22.983 14.0759 22.1768 14.8826L20.1592 16.8926C19.3516 17.6989 19.3482 19.0103 20.1505 19.8207L21.3035 18.689C21.1868 18.3284 21.3304 17.9153 21.6159 17.6295L22.8995 16.3519C23.3061 15.9462 23.9584 15.9491 24.3595 16.3543L25.4513 17.458C25.8528 17.8628 25.8511 18.5171 25.447 18.9232L24.1634 20.2024C23.8918 20.473 23.4461 20.6217 23.1002 20.5263L21.9709 21.6589C22.7745 22.4718 24.0803 22.4747 24.8889 21.6684L26.9039 19.6592C27.7141 18.8525 27.7167 17.5398 26.9131 16.7282ZM19.5261 24.0918C19.6219 24.4441 19.4686 24.8997 19.1909 25.1777L17.9923 26.3752C17.5807 26.7845 16.916 26.7833 16.5067 26.369L15.393 25.2475C14.9847 24.8349 14.9873 24.1633 15.3982 23.7547L16.598 22.5577C16.8903 22.2661 17.3104 22.1202 17.6771 22.2438L18.8335 21.0715C18.0149 20.2462 16.6825 20.2421 15.8606 21.0632L13.9152 23.0042C13.0923 23.8266 13.0884 25.1629 13.9065 25.9886L15.7582 27.8618C16.576 28.6846 17.9072 28.6912 18.7311 27.8701L20.6765 25.9287C21.4985 25.1054 21.5024 23.7717 20.6855 22.9443L19.5261 24.0918ZM19.2579 23.5631C18.9801 23.8419 18.5343 23.8411 18.2618 23.5581C17.9879 23.2743 17.9901 22.8204 18.2661 22.5399L21.5907 19.1611C21.8668 18.8823 22.3117 18.8831 22.5851 19.164C22.8605 19.4457 22.8588 19.9009 22.5817 20.183L19.2579 23.5631Z"
                        fill="#1FDAB5" />
                  </svg></a>
               <a class="portfolio-link__btn" href="javascript://">
                  <svg width="41" height="41" viewBox="0 0 41 41" fill="none" xmlns="http://www.w3.org/2000/svg">
                     <path fill-rule="evenodd" clip-rule="evenodd"
                        d="M20.5973 0.997925C31.8653 0.997925 40.9999 9.95225 40.9999 20.9979C40.9999 32.0432 31.8653 40.9979 20.5973 40.9979C9.32922 40.9979 0.194641 32.0432 0.194641 20.9979C0.194641 9.95225 9.32922 0.997925 20.5973 0.997925Z"
                        fill="#18CFAB" />
                     <rect x="14" y="16" width="12" height="11" fill="white" />
                  </svg></a>
               <p class="portfolio-style_title">Creative design</p>
               <p class="portfolio-style_group">${img.data}</p>
            </div>
      `);

            count++;

            if (count === images.length) {
               btnAddMore.remove();
               spanLoader.remove();
            }

         })
      }, 5000);
   })
}

document.addEventListener('DOMContentLoaded', () => {
   Services();
   Portfolio();
});

// Swiper slider

const swiper = new Swiper(".thumbs-slider", {
   spaceBetween: 4,
   slidesPerView: 4,
   watchSlidesProgress: true,
});
const swiper2 = new Swiper(".slider", {
   loop: true,
   spaceBetween: 4,
   navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
   },
   thumbs: {
      swiper: swiper,
   },
});

// Masonry
const elem = document.querySelector('.grid');
const msnry = new Masonry(elem, {
   itemSelector: '.grid-item',
   columnWidth: '.grid-item',
   gutter: 20,
   horizontalOrder: true,
   fitWidth: true,
});

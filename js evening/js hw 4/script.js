"use strict";

function isNumeric(val) {
   if (typeof val === "number" && !Number.isNaN(val) && val !== Infinity && val !== -Infinity) {
      return true;
   }

   return false;
}

isNumeric();


let a,
   b,
   c,
   res;

do {
   a = Number(prompt("Enter the first number", a));
   b = Number(prompt("Enter the second number", b));
   c = prompt("Enter a sign +, -, *, /");

} while (!isNumeric(a) || !isNumeric(b));

function calcArguments(a, b, c) {

   switch (c) {
      case "+":
         res = a + b;
         break;
      case "-":
         res = a - b;
         break;
      case "*":
         res = a * b;
         break;
      case "/":

         if (b !== 0) {
            res = a / b;
            break;
         } else {
            throw new Error("Do not divide by 0");
         }
   }
};

calcArguments(a, b, c);
alert(`${a} ${c} ${b} = ${res}`);
"use strict";

const password = document.querySelector(".password"),
   passwordRepeat = document.querySelector(".repeat-password"),
   passwordIcon = document.querySelector(".icon-password"),
   passwordRepeatIcon = passwordRepeat.nextElementSibling,
   btn = document.querySelector(".btn"),
   wrongPassword = document.createElement("span");


const toggleIcon = (icon) => {
   icon.classList.toggle("fa-eye");
   icon.classList.toggle("fa-eye-slash");
}

const replaceIcon = () => {
   if (password.getAttribute("type") === "password") {
      password.setAttribute("type", "text");
      toggleIcon(passwordIcon);
   } else {
      password.setAttribute("type", "password");
      toggleIcon(passwordIcon);
   }
}

const replaceRepeatIcon = () => {
   if (passwordRepeat.getAttribute("type") === "password") {
      passwordRepeat.setAttribute("type", "text");
      toggleIcon(passwordRepeatIcon);
   } else {
      passwordRepeat.setAttribute("type", "password");
      toggleIcon(passwordRepeatIcon);
   }
}

const onClick = (event) => {
   event.preventDefault();
   if (password.value === passwordRepeat.value &&
      password !== "") {
      alert("You are welcome!");
   } else {
      wrongPassword.textContent = "Нужно ввести одинаковые значения!";
      wrongPassword.style.color = "red";
      passwordRepeat.after(wrongPassword);
   }
}



passwordIcon.addEventListener("click", replaceIcon);
passwordRepeatIcon.addEventListener("click", replaceRepeatIcon);
btn.addEventListener("click", onClick);
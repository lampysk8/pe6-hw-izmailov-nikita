"use strict";

let btns = document.querySelectorAll(".btn");

document.addEventListener("keydown", changeLetters);

function changeLetters(e) {
   btns.forEach(element => {
      if (element.innerHTML.toLowerCase() === e.key.toLowerCase()) {
         btns.forEach(btn => {
            btn.classList.remove("blue");
         })
         element.classList.add("blue");
      }
   });
}

"use strict";

function filterBy(arr, type) {
   let res = [];

   arr.forEach(el => {
      if (typeof el !== type) {
         res.push(el);
      }
   });

   return res;

}
console.log(filterBy(["22", null, 12, "33"], "string"));
console.log(filterBy(["22", null, 12, "33"], "number"));
console.log(filterBy(["22", true, 12, "33"], "boolean"));

"use strict";

let userNum = +prompt("Enter the number?");

for (let i = 1; i <= userNum; i++) {
   if (i % 5 === 0) {
      console.log(i);
   } else if (userNum < 5) {
      console.log("Sorry, no numbers");
   }
}

// ! Mastering level:)

let min;
let max;

do {
   min = +prompt("Enter your minimal number", 2);
   max = +prompt("Enter your bigger number");
} while (
   Number.isInteger(min) !== true ||
   Number.isInteger(max) !== true ||
   isNaN(+min) || isNaN(+max) ||
   min > max ||
   min < 2);

oneMoreTime:
for (let i = min; i <= max; i++) {
   for (let j = min; j < i; j++) {
      if (i % j === 0) continue oneMoreTime;
   }

   console.log(i);
}






// let min = +prompt("Enter the number", 2);
// let max = +prompt("Enter the number");

// for (let i = min; i <= max; i++) {
//    if (i % min === 1) {
//       console.log(i);
//    }
// }

// function isPrime(num) {
//    for (let i = 2; i < num; i++) {
//       if (num % i === 0) {
//          return false;
//       }
//    }
//    return num > 1;
// }
// console.log(isPrime(2)); //false
// console.log(isPrime(3)); //true
// console.log(isPrime(4)); //true
// console.log(isPrime(5)); // false
// console.log(isPrime(6));
// console.log(isPrime(7));
// console.log(isPrime(8));
// console.log(isPrime(9));
// console.log(isPrime(10));
// console.log(isPrime(11));
// console.log(isPrime(12));
// console.log(isPrime(13));
// console.log(isPrime(14));
// console.log(isPrime(15)); 

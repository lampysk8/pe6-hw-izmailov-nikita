"use strict";

const name = prompt("Enter your firstname"),
   surName = prompt("Enter your lastname");

function createNewUser(name, surName) {

   let res = {
      firstName: name,
      lastName: surName,
      getLogin() {
         return (this.firstName.slice(0, 1) + "" + this.lastName).toLowerCase();
      },
   };

   return res;
}

const newUser = createNewUser(name, surName);


console.log(newUser);
console.log(newUser.firstName);
console.log(newUser.lastName);
console.log(newUser.getLogin());



let obj = Object.create({}, {
   "_firstName": {
      value: "mik",
      writable: true,
   },
   "firstName": {
      get() {
         return this._firstName;
      }
   },
   "setFirstName": {
      set(value) {
         this._firstName = value;
      }
   },
})

console.log(obj.firstName);

obj._firstName = "vas";
console.log(obj.firstName);
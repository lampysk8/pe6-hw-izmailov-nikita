"use strict";


const btns = document.querySelectorAll(".tabs-title"),
   tabs = document.querySelectorAll(".tabs-item");

btns.forEach(element => {
   element.addEventListener("click", () => {
      let currentBtn = element;
      let tabId = currentBtn.getAttribute("data-tab");
      console.log(tabId);
      let currentTab = document.querySelector(tabId);
      console.log(currentTab);

      if (!currentBtn.classList.contains("active")) {

         btns.forEach(element => {
            element.classList.remove("active");
         });

         tabs.forEach(element => {
            element.style.display = "none";
         });
         currentBtn.classList.add("active");
         currentTab.style.display = "block";
      }
   });
});

document.querySelector(".tabs-title").click();

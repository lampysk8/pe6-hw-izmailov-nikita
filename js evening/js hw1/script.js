"use strict";

// 1.Как можно объявить переменную в Javascript ?

//тремя способами let, const, var.

//    2.В чем разница между функцией prompt и функцией confirm ?

// prompt предлагает ввести данные, а confirm лишь сделать выбор между OK / CANCEL

//       3.Что такое неявное преобразование типов ? Приведите один пример.

//неявное преобразование типов - это когда в выражениях используют значения различных типов: 1 == null, 2 + "4".

// ! Homework 1

// 1.Объявите две переменные: admin и name.Установите значение переменной name в ваше имя.Скопируйте это значение в переменную admin и выведите его в консоль.

let admin;
let userName = "Nikita";
admin = userName;

console.log(admin);

// 2.Объявите переменную days и проинициализируйте ее числом от 1 до 10. Преобразуйте это число в количество секунд и выведите в консоль.

for (let days = 1; days <= 10; days++) {
   console.log(days);
   days *= 24 * 60 * 60;
}

let allDays;

for (let i = 1; i <= 10; i++) {
   console.log(i);
   allDays = i * 24 * 60 * 60;
}
console.log(allDays);

// 3.Запросите у пользователя какое либо значение и выведите его в консоль.

let userRequest = prompt("Any request", "");

console.log(userRequest);




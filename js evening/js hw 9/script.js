"use strict";

function addArraysToList(arr, parentElem = document.body) {
   const list = document.createElement("ul");

   arr.forEach(element => {
      const item = document.createElement("li");

      if (Array.isArray(element)) {
         addArraysToList(item, list);
      } else {
         item.textContent = element;
      }

      list.append(item);
   });

   parentElem.append(list);
}

addArraysToList(["12", "true", "Hello", true]);

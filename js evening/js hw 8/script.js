"use strict";

let paragraphs = document.querySelectorAll("p"),
   list = document.querySelector("#optionsList"),
   testParagraph = document.querySelector("#testParagraph"),
   headerElements = document.querySelector(".main-header"),
   headerElChildren = Array.from(headerElements.children),
   optionTitle = document.querySelectorAll(".options-list-title");

paragraphs.forEach(element => {
   element.style.backgroundColor = "#ff0000";
});
console.log("----1----");

console.log(paragraphs);

console.log("----2----");

console.log(list);

console.log(`Свойство ниже parentElement возвращает родитель-элемент`);
console.log(list.parentElement);

console.log(`Свойство ниже parentNode возвращает «любого родителя»`);
console.log(list.parentNode);

console.log(`Свойство ниже childNodes возвращает коллекцию нод?! данного элемента`);
console.log(list.childNodes);

let childNode = list.childNodes;
for (let i = 0; i < childNode.length; ++i) {
   console.log(childNode[i].nodeName, childNode[i].nodeType);
}

console.log(`Свойство ниже children возвращает коллекцию элементов данного элемента`);
console.log(list.children);

let childElem = list.children;
for (let i = 0; i < childElem.length; ++i) {
   console.log(childElem[i].nodeName, childElem[i].nodeType);
}

console.log("----3----");

testParagraph.innerText = "This is a paragraph";
console.log(testParagraph);

console.log("----4----");

console.log(headerElements.children);

console.log("----5----");

headerElChildren.forEach(element => {
   element.classList.add("nav-item");
   console.log(element);
});

console.log("----6----");

console.log(optionTitle);

optionTitle.forEach(element => {
   element.classList.remove("options-list-title");
   console.log(element);
});




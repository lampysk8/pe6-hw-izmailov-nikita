"use strict";

// let btnWrapper = document.createElement("div"),
//    stopBtn = document.createElement("button"),
//    playBtn = document.createElement("button");

// images.after(btnWrapper);
// btnWrapper.classList.add("btns");

// playBtn.classList.add("btn");
// playBtn.innerText = "Play again";
// btnWrapper.prepend(playBtn);

// stopBtn.classList.add("btn");
// stopBtn.innerText = "Stop";
// btnWrapper.prepend(stopBtn);


// imagesList.forEach((element) => {
//    element.classList.add("hidden");
// })

// console.log(imagesList);

// let onShowImage = setInterval()


const imagesList = document.querySelectorAll(".image-to-show"),
   imgWrapper = document.querySelector(".images-wrapper");

imgWrapper.insertAdjacentHTML("afterend",
   `<div class="btns">
<button class="btn">Stop</button>
<button class="btn">Play again</button>
</div>`);

let playBtn = document.querySelector(".btn"),
   stopBtn = playBtn.nextElementSibling;

let index = 0;
let timer;

function changeImg() {
   timer = setInterval(() => {
      imagesList.forEach((el, i) => {
         if (el.classList.contains("active")) {
            if (i === imagesList.length - 1) {
               index = 0;
            } else {
               index = i + 1;
            }
         }
         el.classList.remove("active");
      });
      imagesList[index].classList.add("active");
   }, 3000);
}
changeImg();

playBtn.addEventListener("click", () => clearInterval(timer));
stopBtn.addEventListener("click", () => {
   clearInterval(timer);
   changeImg();
});
"use strict";
let userAge;
let userName;

do {
   userName = prompt(`How is your name?`, userName);
   userAge = +prompt(`How old are you?`, userAge);

} while (userName === "" ||
userAge === "" ||
isNaN(+userAge) ||
userAge === "" ||
userName === null ||
   userAge === null);

if (userAge < 18) {
   alert("You are not allowed to visit this website");
} else if (userAge > 18 && userAge <= 22) {
   if (confirm("Are you sure you want to continue?")) {
      alert(`Welcome ${userName}`);
   } else {
      alert("You are not allowed to visit this website");
   }
} else {
   alert(`Welcome ${userName}`);
}
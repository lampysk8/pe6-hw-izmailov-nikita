import { Component } from "react";

import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

import "./app.scss"

export default class App extends Component {

	state = {
		openModalFirst: false,
		openModalSecond: false,
	};

	closeModal() {
		this.setState({
			openModalFirst: false,
			openModalSecond: false,
		})
	}

	render() {

		const { openModalFirst, openModalSecond } = this.state;

		return (
			<div className="App" >
				<Button
					bgColor="#b3382c"
					text={"Open first modal"}
					onClick={() => this.setState({ openModalFirst: true })}
				/>
				<Button
					bgColor="#b2902d"
					text={"Open second modal"}
					onClick={() => this.setState({ openModalSecond: true })}
				/>
				{openModalFirst && (
					<Modal
						closeButton={true}
						header="Do you really want to delete this file?"
						text="Lorem ipsum dolor sit amet consectetur, adipisicing elit. Distinctio, repellendus?"
						closeModal={() => this.closeModal()}
						actions={true}
					/>
				)}
				{openModalSecond && (
					<Modal
						closeButton={false}
						header="How are you?"
						text="Lorem ipsum dolor sit amet consectetur, adipisicing elit. "
						closeModal={() => this.closeModal()}
						actions={true}
					/>
				)}
			</div >
		);
	}

}


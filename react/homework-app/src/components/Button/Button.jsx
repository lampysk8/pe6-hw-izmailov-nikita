import { Component } from "react";
import "./button.scss";

export default class Button extends Component {
	render() {
		const { text, bgColor, onClick } = this.props;

		return (
			<>
				<button style={{ backgroundColor: bgColor }} className="btn" onClick={onClick}>
					{text}
				</button>
			</>
		);
	}
}

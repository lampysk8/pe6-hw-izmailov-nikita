import { Component } from "react";
import Button from "../Button/Button";

import "./Modal.scss";




export default class Modal extends Component {

	render() {
		const { header, text, closeModal, closeButton, actions} = this.props;

		return (
			<div className="bg active" onClick={closeModal}>
				<div className="modal" onClick={(e) => e.stopPropagation()}>
					<div className="modal__header">
						<h2>{header}</h2>
						{closeButton && (
							<span onClick={closeModal}>&times;</span>
						)}
					</div>
					<div className="modal__body">
						<p>{text}</p>
					</div>
					<div className="modal__wrapper">
						{actions && (
						<>
						<Button
							bgColor="#b3382c"
							text={"Ok"}
							onClick={() => console.log("Ok")} />
						<Button
							bgColor="#b3382c"
							text={"Cancel"}
							onClick={() => closeModal()} />
						</>
							)}
					</div>
				</div>
			</div>
		);
	}
}

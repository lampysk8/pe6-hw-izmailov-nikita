import { Component } from "react";
import { ReactComponent as Cart } from './cart.svg';
import { ReactComponent as Logo } from './logo.svg';
import { ReactComponent as Favorite } from "./favorite.svg";

import "./appHeader.scss";

class AppHeader extends Component {

   render() {

      const { increaseCart, increaseFavorite } = this.props;

      return (
         <div className="header">
            <div className="header__logo">
               <div className="header__logo-container">
                  <a href="#s">
                     <Logo width="40" height="40" style={{}} />
                  </a>
               </div>
            </div>
            <div className="header__icons">
               <div className="header__icons-container--favorite">
                  <a href="#s">
                     <Favorite width="35" height="35" />
                     {increaseFavorite !== 0 && <span className="header__icons-container--span">{increaseFavorite}</span>}
                  </a>
               </div>
               <div className="header__icons-container--cart">
                  <a href="#s">
                     <Cart width="40" height="40" />
                     {increaseCart !== 0 && <span className="header__icons-container--span">{increaseCart}</span>}
                  </a>
               </div>
            </div>
         </div>
      )
   }
}
export default AppHeader;
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ReactComponent as Star } from './star.svg'

import Button from '../Button/Button';

import "./card.scss";

class Card extends Component {

   state = {
      favorite: false,
   }
   componentDidMount() {
      const favorites = JSON.parse(localStorage.getItem("favorite") || "[]");
      favorites.forEach((el) => {
         if (el.article === this.props.article) {
            this.setState({ favorite: true });
         }
      });
   }

   setFavorite = () => {
      this.setState(({ favorite }) => ({ favorite: !favorite }));
   };

   render() {

      const { favorite } = this.state;

      const { onAddToFavorite, onAddToCart, showModal, ...itemProps } = this.props;
      const { article, imgUrl, name, color, price, descr, } = itemProps;

      return (
         <div id={article} className="card" color={color}>
            <img src={imgUrl} alt="some__picture" className="card__img" />
            <div className="card__body">
               <div className="card__body-wrapper">
                  <h2 className="card__body-wrapper--title">{name}</h2>
                  <div className="card__body-wrapper--icon">
                     <Star className={favorite ? "active" : "star"}
                        width="20"
                        height="20"
                        onClick={() => {
                           onAddToFavorite(itemProps);
                           this.setFavorite();
                        }}
                     />
                  </div>
               </div>
               <p className="card__body-description">
                  {`${descr.slice(0, 52)}...`}
               </p>
            </div>
            <div className="card__footer">
               <div className="card__footer-price">{price}</div>
               <Button
                  className="card__footer-btn"
                  bgColor="#1e1e20"
                  text={"Add to cart"}
                  onClick={() => onAddToCart(itemProps)}
               />
            </div>
         </div >
      )
   }
}

Card.propTypes = {
   favorite: PropTypes.bool
};

export default Card;
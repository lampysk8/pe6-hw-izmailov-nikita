
import React, { Component } from 'react';


import Card from '../Card/Card';

import "./cardList.scss"

class CardList extends Component {

   render() {


      const { onAddToFavorite, showModal, onAddToCart } = this.props;
      const { cards } = this.props;


      const elements = cards.map((card, i) => {
         const { ...itemProps } = card;
         return (
            <Card key={i}
               onAddToFavorite={onAddToFavorite}
               onAddToCart={onAddToCart}
               showModal={showModal}
               {...itemProps}
            />
         )
      })


      return (
         <ul className='list' >
            {elements}
         </ul>
      );
   }
}

export default CardList;

import { Component } from "react";
import PropTypes from 'prop-types';


import AppHeader from "./components/AppHeader/AppHeader";
import CardList from "./components/CardList/CardList";
import Modal from "./components/Modal/Modal";

import sendRequest from './services/sendRequest';


import "./app.scss"



class App extends Component {
	constructor(props) {
		super(props)
		this.state = {
			openModal: false,
			newGoods: null,
			favorite: [],
			cart: [],
			cards: []
		}
	}

	componentDidMount = () => {
		sendRequest('./array.json')
			.then(data => {
				this.setState({
					cards: data
				})
			}, [])

		const localCart = JSON.parse(localStorage.getItem("cart") || "[]");
		this.setState({ cart: localCart });

		const localFav = JSON.parse(localStorage.getItem("favorite") || "[]");
		this.setState({ favorite: localFav });
	}


	onAddToFavorite = (newFav) => {
		const data = this.state.favorite.filter(({ article }) => article !== newFav.article);
		if (data.length !== this.state.favorite.length) {
			localStorage.setItem("favorite", JSON.stringify(data));
			this.setState({ favorite: data });
			return;
		}

		const localFav = [...this.state.favorite, newFav];
		localStorage.setItem("favorite", JSON.stringify(localFav));
		this.setState({ favorite: localFav });
	};

	onAddToCart = (newGoods) => {
		this.showModal();
		this.setState({ newGoods })
	}

	confirmAddToCart = () => {
		const { newGoods } = this.state;

		if (newGoods) {
			const localCart = [...this.state.cart, newGoods];
			localStorage.setItem("cart", JSON.stringify(localCart));

			this.setState(({ cart }) => ({
				cart: [...cart, newGoods],
			}));
		}
	};

	showModal = () => {
		this.setState({ openModal: true })
	}

	closeModal = () => {
		this.setState({ openModal: false });
	}

	render() {

		const { cards, cart, favorite, newGoods } = this.state;

		return (
			<div className="App" >
				<AppHeader
					increaseCart={cart.length}
					increaseFavorite={favorite.length}
				/>
				<CardList
					cards={cards}
					showModal={this.showModal}
					onAddToFavorite={this.onAddToFavorite}
					onAddToCart={this.onAddToCart}
				/>
				{this.state.openModal &&
					<Modal
						closeButton={true}
						header="Do you really want to add this item?"
						text={`
							Artist: ${newGoods.name}! 
							Price: ${newGoods.price}
						`}
						closeModal={() => this.closeModal()}
						confirmAddToCart={() => this.confirmAddToCart()}
						actions={true}
					/>
				}
			</div >
		);
	}
}

App.propTypes = {
	openModal: PropTypes.bool,
	newGoods: PropTypes.number,
	favorite: PropTypes.array,
	cart: PropTypes.array,
	cards: PropTypes.array,
};

export default App;